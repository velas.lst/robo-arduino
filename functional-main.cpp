#include <ArduinoJson.h>
#include <Servo.h>
#include <Stepper.h>

int pasosPorVuelta = 2048;
Stepper motor(pasosPorVuelta, 8, 10, 9, 11);

// int potVal;//valor del potenciómetro
int servoMotPin = 3; //entrada digital del servomotor
int pinAzul = 12;
int pinRojo = 13;

void setup()
{
    Serial.begin(9600);
    motor.setSpeed(5);
}

void loop()
{
  
  if (Serial.available() > 0) {    
    String info = Serial.readString(); 
    int npin = info.substring(0, 2).toInt();
    String valPin = info.substring(3);
    Serial.println("npin = " + String(npin));
    Serial.println("valPin = " + valPin);
    if (npin == servoMotPin) { // usamos el servo motor
      manejarServoMotor(valPin);
    }
    if (npin == pinAzul) { // usamos el Led Azul
        manejarLedAzul(valPin);
    }
    if (npin == pinRojo) { // usamos el Led rojo
        manejarLedRojo(valPin);
    }
  }
  delay(1000);
}


void manejarServoMotor(String valor) {
    int grados = (valor).toInt();
    int pasos = map (grados, 0, 360, 0, pasosPorVuelta);
    motor.step(pasos);
}

void manejarLedAzul(String valor) {

}

void manejarLedRojo(String valor) {

}